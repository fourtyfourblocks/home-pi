/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.async;

/**
 * date        : 29.11.13
 * author      : pawel
 * file name   : AsyncResponseHandler
 * <p/>
 * description :
 */
public interface AsyncResponseHandler<T>
{
    void resume(T value);

    void reject(Throwable reason);

    void cancel(int retryAfter);

    Class<T> getType();
}
