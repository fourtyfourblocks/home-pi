/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.utils.cdi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

/**
 * date        : 03.02.14
 * author      : pawel
 * file name   : SystemPropertyFactory
 * <p/>
 * description :
 */
public class SystemPropertyFactory
{

    private static final Logger log = LoggerFactory.getLogger(SystemPropertyFactory.class);

    @Produces
    @SystemProperty("")
    public String produce(InjectionPoint ip)
    {
        SystemProperty sp = ip.getAnnotated().getAnnotation(SystemProperty.class);
        if (sp == null)
        {
            return null;
        }
        String property = getSystemProperty(sp);
        if (sp.required() && "".equals(property))
        {
            throw new IllegalStateException("No Property found for key=" + sp.value());
        }

        return property;

    }

    String getSystemProperty(SystemProperty sp)
    {
        return System.getProperty(sp.value(), sp.def());
    }
}