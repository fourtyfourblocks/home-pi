/*
 * Copyright (c) 2013 SmartRecruiters Inc. All Rights Reserved.
 */

package com.fourtyfourblocks.thermomaster.akka.message;

import com.fourtyfourblocks.akka.messages.Message;
import com.fourtyfourblocks.async.AsyncResponseHandler;

import java.util.Collection;

/**
 * Date: 24.12.2013
 * Time: 10:19
 *
 * @Author pawel kaminski
 */
public abstract class AsyncResponseMessage extends Message
{

    private static final long serialVersionUID = -2909127965099244307L;

    private final AsyncResponseHandler handler;

    public AsyncResponseMessage(AsyncResponseHandler handler)
    {
        this.handler = handler;
    }

    @SuppressWarnings("unchecked")
    public <T> AsyncResponseHandler<T> getResponseHandler(Class<T> clazz)
    {
        return (AsyncResponseHandler<T>) handler;
    }

    @SuppressWarnings("unchecked")
    public <T> AsyncResponseHandler<Collection<T>> getListResponseHandler(Class<T> clazz)
    {
        return (AsyncResponseHandler<Collection<T>>) handler;
    }
}
