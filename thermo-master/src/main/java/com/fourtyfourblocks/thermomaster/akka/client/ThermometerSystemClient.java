/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.client;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.fourtyfourblocks.async.AsyncResponseHandler;
import com.fourtyfourblocks.thermomaster.akka.message.GetStatusResponseMessage;
import com.fourtyfourblocks.thermomaster.akka.message.SynchronizeDevicesMessage;
import com.fourtyfourblocks.thermomaster.domain.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;

/**
 * date        : 24.11.13
 * author      : pawel
 * file name   : ThermometerSystemClient
 * <p/>
 * description :
 */
public class ThermometerSystemClient
{
    private final static Logger logger = LoggerFactory.getLogger(ThermometerSystemClient.class);

    @Inject
    @Named("thermometerSuper")
    private ActorRef master;

    @Inject
    @Named("thermometerSystem")
    private ActorSystem theSystem;

    public void getStatus(final AsyncResponseHandler<Collection<Status>> response)
    {
        logger.info("Fetching devices status.");
        master.tell(new GetStatusResponseMessage(response), null);
    }

    public void synchronize()
    {
        logger.info("Requesting synchronizing with devices.");
        master.tell(new SynchronizeDevicesMessage(), null);
    }
}
    
