/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.guice;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.fourtyfourblocks.akka.di.DiAwareCreatorFactory;
import com.fourtyfourblocks.akka.di.guice.BaseModule;
import com.fourtyfourblocks.thermomaster.akka.actor.ThermometerSuperActor;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Stage;
import com.google.inject.name.Names;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;

/**
 * date        : 13.09.13
 * author      : pawel
 * file name   : CdiAwareWorld
 * <p/>
 * description :
 */
public class GuiceAwareWorld extends AbstractModule
{
    private final static Logger logger = LoggerFactory.getLogger(GuiceAwareWorld.class);
    private ActorRef superActor;

    public Injector initialize(Module... module)
    {
        return Guice.createInjector(Stage.DEVELOPMENT, new BaseModule(), this);
    }

    @Provides
    @Named("thermometerSuper")
    public ActorRef getSuperActorRef(@Named("thermometerSystem") ActorSystem system, DiAwareCreatorFactory factory)
    {
        logger.info("initializing ThermometerMasterActor");
        if (superActor == null)
        {
            final Props props = Props.create(factory.creator(ThermometerSuperActor.class));
            superActor = system.actorOf(props, "thermometerSuper");
        }
        return superActor;
    }

    @Override
    protected void configure()
    {
        bind(ActorSystem.class).annotatedWith(Names.named("thermometerSystem")).toInstance(ActorSystem.create("thermometer-system"));
    }
}
    
