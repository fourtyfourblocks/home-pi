/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.actor;

import akka.actor.ActorRef;
import akka.actor.ReceiveTimeout$;
import com.fourtyfourblocks.akka.di.DiAwareActor;
import com.fourtyfourblocks.thermomaster.akka.message.DeviceInfoMessage;
import com.fourtyfourblocks.thermomaster.akka.message.ResponseStatusMessage;
import com.fourtyfourblocks.thermomaster.domain.FileDevice;
import com.fourtyfourblocks.thermomaster.domain.Status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * date        : 24.11.13
 * author      : pawel
 * file name   : ThermometerSlaveActor
 * <p/>
 * description :
 */
public class ThermometerSlaveActor extends DiAwareActor
{
    private FileDevice device;
    private ActorRef master;

    @Override
    public void onMessage(Object message) throws Exception
    {
        if (is(message, DeviceInfoMessage.class))
        {
            master = getSender();
            final DeviceInfoMessage info = cast(message, DeviceInfoMessage.class);
            device = cast(info.getDevice(), FileDevice.class);

            final Status status = readTemperature(device);
            tell(master, new ResponseStatusMessage(status));
        }
        else if (is(message, ReceiveTimeout$.class))
        {
            final Status status = readTemperature(device);
            tell(master, new ResponseStatusMessage(status));
        }
        else
        {
            unhandled(message);
        }
    }

    private Status readTemperature(FileDevice device) throws IOException
    {
        final List<String> temperature = Files.readAllLines(device.getLocation(), Charset.defaultCharset());
        logger.info("Read temperature {}", temperature.get(1));
        logger.info("Read crc {}", temperature.get(0));

        requestTimeout(5, TimeUnit.MINUTES);

        return new Status(temperature.get(1), "ON", device.getName());
    }
}
    
