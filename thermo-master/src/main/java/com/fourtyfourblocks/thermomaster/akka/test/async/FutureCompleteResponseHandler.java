/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.test.async;

import akka.dispatch.OnComplete;
import com.fourtyfourblocks.async.AsyncResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * date        : 29.11.13
 * author      : pawel
 * file name   : FutureCompleteResponseHandler
 * <p/>
 * description :
 */
public class FutureCompleteResponseHandler<T> extends OnComplete<Object>
{
    private final static Logger logger = LoggerFactory.getLogger(FutureCompleteResponseHandler.class);
    private final AsyncResponseHandler<T> asyncResponse;

    public FutureCompleteResponseHandler(AsyncResponseHandler<T> asyncResponse)
    {
        this.asyncResponse = asyncResponse;
    }

    @Override
    public void onComplete(Throwable throwable, Object o) throws Throwable
    {
        logger.debug("handling response with value=[{]] throwable={}", o, throwable);
        final Class<T> type = asyncResponse.getType();
        if (is(o, type))
        {
            asyncResponse.resume(type.cast(o));
        }
        else if (throwable != null)
        {
            asyncResponse.reject(throwable);
        }
    }

    public <T> boolean is(Object message, Class<T> clazz)
    {
        return message != null && (message.getClass() == clazz || clazz.isInstance(message));
    }
}
    
