/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.actor;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.SupervisorStrategy;
import com.fourtyfourblocks.akka.di.ActorProps;
import com.fourtyfourblocks.akka.di.DiAwareActor;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import static com.fourtyfourblocks.akka.SupervisorStrategyFactory.oneForOne;
import static com.fourtyfourblocks.akka.SupervisorStrategyFactory.restartDecider;

/**
 * date        : 18.05.14
 * author      : pawel
 * file name   : ThermometerSuperActor
 * <p/>
 * description :
 */
public class ThermometerSuperActor extends DiAwareActor
{
    private final static Logger logger = LoggerFactory.getLogger(ThermometerSuperActor.class);

    @Inject
    @ActorProps(actorClass = ThermometerMasterActor.class)
    Props masterProps;

    private ActorRef master;

    @Override
    public void preStart() throws Exception
    {
        logger.info("Starting Master actor");
        master = create(masterProps, "master");
    }

    @Override
    public SupervisorStrategy supervisorStrategy()
    {
        return oneForOne(5, 1, restartDecider);
    }

    @Override
    public void onMessage(Object message) throws Exception
    {
        master.forward(message, getContext());
    }

    @VisibleForTesting
    public ActorRef getMaster()
    {
        return master;
    }
}
    
