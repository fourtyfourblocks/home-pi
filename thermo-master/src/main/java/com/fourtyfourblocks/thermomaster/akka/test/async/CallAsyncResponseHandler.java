/*
 * Copyright (c) 2013 SmartRecruiters Inc. All Rights Reserved.
 */

package com.fourtyfourblocks.thermomaster.akka.test.async;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import com.fourtyfourblocks.async.AsyncResponseHandler;
import com.fourtyfourblocks.asyncclient.akka.ActorResponseHandler;

/**
 * Date: 24.12.2013
 * Time: 10:51
 *
 * @Author pawel kaminski
 */
public class CallAsyncResponseHandler<T> extends ActorResponseHandler<T>
{

    private final AsyncResponseHandler<T> responseHandler;

    public CallAsyncResponseHandler(UntypedActor caller, AsyncResponseHandler<T> responseHandler)
    {
        super(caller);
        this.responseHandler = responseHandler;
    }

    @Override
    public void onSuccess(ActorRef invoker, ActorRef sender, T data)
    {
        onSuccess(invoker, sender, data, responseHandler);
    }

    public void onSuccess(ActorRef invoker, ActorRef sender, T data, AsyncResponseHandler<T> response)
    {
        response.resume(data);
    }

    @Override
    public void onError(ActorRef sender, Object error)
    {
        onError(sender, error, responseHandler);
    }

    private void onError(ActorRef sender, Object error, AsyncResponseHandler<T> responseHandler)
    {
        responseHandler.reject(new IllegalStateException(error.toString()));
    }
}
