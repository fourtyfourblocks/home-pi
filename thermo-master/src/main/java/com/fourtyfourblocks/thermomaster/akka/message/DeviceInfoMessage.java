/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.message;

import com.fourtyfourblocks.akka.messages.Message;
import com.fourtyfourblocks.thermomaster.domain.Device;

/**
 * date        : 05.02.14
 * author      : pawel
 * file name   : DeviceInfoMessage
 * <p/>
 * description :
 */
public class DeviceInfoMessage extends Message
{
    private static final long serialVersionUID = 8371620343815839725L;

    private final Device device;

    public DeviceInfoMessage(Device device)
    {
        this.device = device;
    }

    public Device getDevice()
    {
        return device;
    }

    @Override
    protected StringBuilder appendToString(StringBuilder sb)
    {
        return sb.append("device=").append(device);
    }
}
    
