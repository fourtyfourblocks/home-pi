/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.message;

import com.fourtyfourblocks.async.AsyncResponseHandler;

/**
 * date        : 05.02.14
 * author      : pawel
 * file name   : GetStatusResponseMessage
 * <p/>
 * description :
 */
public class GetStatusResponseMessage extends AsyncResponseMessage
{
    private static final long serialVersionUID = 5490219795248214811L;

    public GetStatusResponseMessage(AsyncResponseHandler handler)
    {
        super(handler);
    }

    @Override
    protected StringBuilder appendToString(StringBuilder sb)
    {
        return sb;
    }
}
    
