/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.message;

import com.fourtyfourblocks.akka.messages.Message;

/**
 * date        : 05.02.14
 * author      : pawel
 * file name   : SynchronizeDevicesMessage
 * <p/>
 * description :
 */
public class SynchronizeDevicesMessage extends Message
{
    private static final long serialVersionUID = 8769143280178717973L;

    @Override
    protected StringBuilder appendToString(StringBuilder sb)
    {
        return sb;
    }
}
    
