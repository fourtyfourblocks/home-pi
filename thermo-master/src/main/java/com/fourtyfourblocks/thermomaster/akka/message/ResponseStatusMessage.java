/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.message;

import com.fourtyfourblocks.akka.messages.Message;
import com.fourtyfourblocks.thermomaster.domain.Status;

/**
 * date        : 05.02.14
 * author      : pawel
 * file name   : ResponseStatusMessage
 * <p/>
 * description :
 */
public class ResponseStatusMessage extends Message
{
    private static final long serialVersionUID = -1493856408940131166L;

    private final Status status;

    public ResponseStatusMessage(Status status)
    {
        this.status = status;
    }

    public Status getStatus()
    {
        return status;
    }

    @Override
    protected StringBuilder appendToString(StringBuilder sb)
    {
        return sb.append("status=").append(status);
    }
}
    
