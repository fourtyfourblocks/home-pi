/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.actor;

import akka.actor.ActorKilledException;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.ReceiveTimeout$;
import akka.actor.SupervisorStrategy;
import com.fourtyfourblocks.akka.Actor;
import com.fourtyfourblocks.akka.Behaviour;
import com.fourtyfourblocks.akka.SupervisorStrategyFactory;
import com.fourtyfourblocks.akka.di.ActorProps;
import com.fourtyfourblocks.akka.di.DiAwareActor;
import com.fourtyfourblocks.async.AsyncResponseHandler;
import com.fourtyfourblocks.thermomaster.akka.message.DeviceInfoMessage;
import com.fourtyfourblocks.thermomaster.akka.message.GetStatusResponseMessage;
import com.fourtyfourblocks.thermomaster.akka.message.ResponseStatusMessage;
import com.fourtyfourblocks.thermomaster.akka.message.SynchronizeDevicesMessage;
import com.fourtyfourblocks.thermomaster.domain.Device;
import com.fourtyfourblocks.thermomaster.domain.Status;
import com.fourtyfourblocks.thermomaster.service.W1ThermometerService;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * date        : 24.11.13
 * author      : pawel
 * file name   : ThermometerMasterActor
 * <p/>
 * description :
 */
public class ThermometerMasterActor extends DiAwareActor
{
    @Inject
    @ActorProps(actorClass = ThermometerSlaveActor.class)
    Props slaveProps;

    @Inject
    W1ThermometerService service;

    private Map<String, Status> statuses;

    @Override
    public SupervisorStrategy supervisorStrategy()
    {
        // NOTE I dont care if one slave breaks, but I dont have a way to reinitialize it with
        // NOTE device info data, and I dont want it to somehow register after creation.
        return SupervisorStrategyFactory.allForOne(0, 1, SupervisorStrategyFactory.suicideDecider);
    }

    @Override
    public void preStart() throws Exception
    {
        logger.info("Initializing...");
        registerDevices();
        become(new DefaultBehaviour(this));
    }

    private void registerDevices() throws IOException
    {
        final List<Device> devices = service.getRegisteredDevices();

        final int size = devices.size();
        if (size == 0)
        {
            requestTimeout(5, TimeUnit.SECONDS);
            logger.info("... could not create any device, postponing initialization.");
        }
        else
        {
            statuses = initializeStatuses(size);
            for (Device device : devices)
            {
                final String id = device.getId();
                final ActorRef slave = create(slaveProps, "slave_" + id);
                tell(slave, new DeviceInfoMessage(device));
            }
            logger.info("... and initialized with {} slaves.", size);
        }
    }

    private Map<String, Status> initializeStatuses(int size)
    {
        if (statuses == null)
        {
            statuses = new HashMap<>(size);
        }

        return statuses;
    }

    private void scanAndRegisterDevices() throws IOException
    {
        service.scan();
        stopTimeout();
        registerDevices();
    }

    private class DefaultBehaviour extends Behaviour
    {
        public DefaultBehaviour(Actor actor)
        {
            super(actor);
        }

        @Override
        public void onMessage(Object message) throws Exception
        {
            if (is(message, ReceiveTimeout$.class))
            {
                scanAndRegisterDevices();
            }
            else if (is(message, GetStatusResponseMessage.class))
            {
                final GetStatusResponseMessage srm = cast(message, GetStatusResponseMessage.class);
                final AsyncResponseHandler<Collection<Status>> response = srm.getListResponseHandler(Status.class);
                response.resume(statuses != null ? statuses.values() : getEmptyStatuses());
            }
            else if (is(message, ResponseStatusMessage.class))
            {
                final ResponseStatusMessage statusMessage = cast(message, ResponseStatusMessage.class);
                final Status currentStatus = statusMessage.getStatus();
                final String deviceId = currentStatus.getDeviceId();

                final Status prevStatus = statuses.get(deviceId);
                statuses.put(deviceId, currentStatus);

                logger.debug("Storing new status for device {}, changing from {}", currentStatus, prevStatus);
            }
            else if (is(message, SynchronizeDevicesMessage.class))
            {
                // restart self
                logger.info("Synchronization requested. Preparing for restarting myself ... ");
                throw new ActorKilledException("Synchronization requested.");
            }
            else
            {
                unhandled(message);
            }
        }

        private Collection<Status> getEmptyStatuses()
        {
            return Collections.emptyList();
        }
    }
}
    
