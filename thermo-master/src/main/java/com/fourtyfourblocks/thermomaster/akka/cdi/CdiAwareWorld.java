/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.cdi;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.fourtyfourblocks.akka.di.DiAwareCreatorFactory;
import com.fourtyfourblocks.akka.di.cdi.CdiAwareCreatorFactory;
import com.fourtyfourblocks.thermomaster.akka.actor.ThermometerMasterActor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * date        : 13.09.13
 * author      : pawel
 * file name   : CdiAwareWorld
 * <p/>
 * description :
 */
@ApplicationScoped
public class CdiAwareWorld
{
    private final static Logger logger = LoggerFactory.getLogger(CdiAwareWorld.class);

    @Inject
    private BeanManager beanManager;

    private DiAwareCreatorFactory factory;
    private ActorSystem system;
    private ActorRef superActorRef;

    @PostConstruct
    public void initialize()
    {
        logger.info("initializing CdiAwareCreatorFactory");
        factory = new CdiAwareCreatorFactory(beanManager);

        logger.info("creating system");
        system = ActorSystem.create("System");

        superActorRef = system.actorOf(Props.create(factory.creator(ThermometerMasterActor.class)), "thermometerSuper");
    }

    @Produces
    public DiAwareCreatorFactory getFactory()
    {
        return factory;
    }

    @Produces
    @Named("thermometerSuper")
    public ActorRef getSuperActorRef()
    {
        return superActorRef;
    }

    @Produces
    @Named("thermometerSystem")
    public ActorSystem getSystem()
    {
        return system;
    }

    /**
     * more info http://stackoverflow.com/questions/8420070/how-to-create-and-destroy-cdi-weld-managed-beans-via-the-beanmanager
     * http://docs.jboss.org/cdi/spec/1.0/html/contexts.html
     */
    @PreDestroy
    public void release()
    {
        logger.info("shutting down pi system");
        system.shutdown();
    }
}
    
