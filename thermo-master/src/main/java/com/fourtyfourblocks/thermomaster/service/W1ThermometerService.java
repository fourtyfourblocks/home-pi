/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.service;

import akka.actor.ActorRef;
import com.fourtyfourblocks.thermomaster.domain.Device;
import com.fourtyfourblocks.thermomaster.domain.FileDevice;
import com.fourtyfourblocks.utils.cdi.SystemProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * date        : 06.02.14
 * author      : pawel
 * file name   : W1ThermometerService
 * <p/>
 * description :
 */
@ApplicationScoped
public class W1ThermometerService
{
    private final static Logger logger = LoggerFactory.getLogger(W1ThermometerService.class);

    @Inject
    @SystemProperty(value = "thermometer.root", def = "/sys/bus/w1/devices")
    String devicesRoot;

    public List<Device> getRegisteredDevices() throws IOException
    {
        final Path slavesDevices = Paths.get(devicesRoot, "w1_bus_master1/w1_master_slaves");
        if (Files.isReadable(slavesDevices))
        {
            final List<String> ids = Files.readAllLines(slavesDevices, Charset.defaultCharset());
            List<Device> devices = new ArrayList<>(ids.size());
            if (ids.size() == 0)
            {
                logger.warn("there are no devices to create. Check {} if devices are setup correctly", slavesDevices.toAbsolutePath());
            }
            else
            {
                for (String id : ids)
                {
                    final Path deviceLocation = Paths.get(devicesRoot, id, "/w1_slave");
                    final FileDevice fileDevice = new FileDevice(id, id, deviceLocation);
                    devices.add(fileDevice);
                }
            }
            return devices;
        }
        else
        {
            throw new IllegalStateException("Cannot create devices. Check " + slavesDevices.toAbsolutePath() + " if devices are setup correctly");
        }
    }

    public void unregisterAllDevices(Set<ActorRef> actors) throws IOException
    {
        final Path slavesDevices = Paths.get(devicesRoot, "w1_bus_master1/w1_master_slaves");
        if (Files.isReadable(slavesDevices))
        {
            final List<String> ids = Files.readAllLines(slavesDevices, Charset.defaultCharset());
            for (String id : ids)
            {
                final Path path = Paths.get(devicesRoot, "w1_bus_master1/w1_master_remove");
                final byte[] scanDirective = id.getBytes(Charset.defaultCharset());
                Files.write(path, scanDirective);
            }
        }
    }

    public void scan() throws IOException
    {
        final Path path = Paths.get(devicesRoot, "w1_bus_master1/w1_master_search");
        final byte[] scanDirective = "1".getBytes(Charset.defaultCharset());
        Files.write(path, scanDirective);
    }
}
    
