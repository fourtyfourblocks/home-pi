/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.domain;

import java.nio.file.Path;

/**
 * date        : 03.02.14
 * author      : pawel
 * file name   : FileDevice
 * <p/>
 * description :
 */
public class FileDevice extends Device
{
    private static final long serialVersionUID = -5275363425975610773L;

    private Path location;

    public FileDevice()
    {
    }

    public FileDevice(String id, String name, Path location)
    {
        super(id, name);
        this.location = location;
    }

    public Path getLocation()
    {
        return location;
    }

    public void setLocation(Path location)
    {
        this.location = location;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("FileDevice{");
        sb.append("id=").append(getId());
        sb.append(", name=").append(getName());
        sb.append(", location=").append(location);
        sb.append('}');
        return sb.toString();
    }
}
    
