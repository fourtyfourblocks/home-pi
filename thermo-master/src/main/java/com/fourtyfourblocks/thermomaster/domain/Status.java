/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.domain;

import java.io.Serializable;

/**
 * date        : 05.02.14
 * author      : pawel
 * file name   : Status
 * <p/>
 * description :
 */
public class Status implements Serializable
{
    private static final long serialVersionUID = 193877039816249904L;

    private String deviceId;
    private String value;
    private String status;

    public Status()
    {
    }

    public Status(String value, String status, String deviceId)
    {
        this.deviceId = deviceId;
        this.value = value;
        this.status = status;
    }

    public String getDeviceId()
    {
        return deviceId;
    }

    public String getStatus()
    {
        return status;
    }

    public String getValue()
    {
        return value;
    }

    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        final StringBuffer sb = new StringBuffer("Status{");
        sb.append("deviceId='").append(deviceId).append('\'');
        sb.append(", value='").append(value).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
    
