/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.domain;

import java.io.Serializable;

/**
 * date        : 03.02.14
 * author      : pawel
 * file name   : Device
 * <p/>
 * description :
 */
public class Device implements Serializable
{
    private static final long serialVersionUID = -132976410564862145L;

    private String id;
    private String name;

    public Device()
    {
    }

    public Device(String id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        final StringBuffer sb = new StringBuffer("Device{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
    
