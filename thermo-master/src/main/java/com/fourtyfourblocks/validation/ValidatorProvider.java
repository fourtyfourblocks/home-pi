/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * date        : 20.09.13
 * author      : pawel
 * file name   : ValidatorProvider
 * <p/>
 * description :
 */
@ApplicationScoped
public class ValidatorProvider
{
    private final static Logger log = LoggerFactory.getLogger(ValidatorProvider.class);

    Validator validator;

    @PostConstruct
    public void initializeValidator()
    {
        log.debug("initializing default bean validator");
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Produces
    @Named("BeanValidator")
    public Validator createValidator()
    {
        return validator;
    }
}