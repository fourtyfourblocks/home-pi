/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.actor;

import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.ReceiveTimeout;
import akka.japi.Creator;
import akka.testkit.JavaTestKit;
import akka.testkit.TestActorRef;
import com.fourtyfourblocks.akka.testing.actor.ForwarderActor;
import com.fourtyfourblocks.thermomaster.domain.FileDevice;
import com.fourtyfourblocks.thermomaster.service.W1ThermometerService;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.nio.file.Paths;
import java.util.Arrays;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

/**
 * date        : 11.03.14
 * author      : pawel
 * file name   : ThermometerMasterActorTest
 * <p/>
 * description :
 */
@RunWith(MockitoJUnitRunner.class)
@Ignore // cannot fully initialize object hierarchy
public class ThermometerMasterActorTest
{
    @Mock
    W1ThermometerService service;

    private ActorSystem system;
    private JavaTestKit probe;
    private TestActorRef<ThermometerMasterActor> actor;

    protected ActorSystem createSystem()
    {
        return ActorSystem.create("System");
    }

    protected JavaTestKit createProbe()
    {
        return new JavaTestKit(system);
    }

    protected TestActorRef<ThermometerMasterActor> createActor(ActorSystem system, final JavaTestKit probe)
    {
        final MockingCreator creator = new MockingCreator(probe, service);
        final Props props = Props.create(creator);

        final TestActorRef<ThermometerMasterActor> actorRef = TestActorRef.create(system, props, "master");
        probe.watch(actorRef);

        return actorRef;
    }

    @Before
    public void setUp() throws Exception
    {
        system = createSystem();
        probe = createProbe();

        doReturn(Arrays.asList()).when(service).getRegisteredDevices();

        actor = createActor(system, probe);
    }

    @After
    public void tearDown() throws Exception
    {
        system.shutdown();
    }

    @Test
    public void initializeMasterActor_deferInitializationForNoDevices() throws Exception
    {
        final FileDevice device = new FileDevice("did", "device 1", Paths.get("/device/path"));
        doReturn(Arrays.asList(device)).when(service).getRegisteredDevices();
        probe.expectNoMsg();

        // trigger timeout
        actor.tell(ReceiveTimeout.getInstance(), probe.getRef());

        verify(service).scan();
    }

    private static class MockingCreator implements Creator<ThermometerMasterActor>
    {
        private static final long serialVersionUID = -1737854259293970815L;
        private final JavaTestKit probe;
        private final W1ThermometerService service;

        public MockingCreator(JavaTestKit probe, W1ThermometerService service)
        {
            this.probe = probe;
            this.service = service;
        }

        @Override
        public ThermometerMasterActor create() throws Exception
        {
            final ThermometerMasterActor actor = new ThermometerMasterActor();
            actor.slaveProps = Props.create(ForwarderActor.class, probe.getRef());
            actor.service = service;

            return actor;
        }
    }
}
    
