/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.actor;

import akka.actor.Props;
import akka.actor.Terminated;
import com.fourtyfourblocks.akka.testing.GuiceTestKitBase;
import com.fourtyfourblocks.akka.testing.actor.ForwarderActor;
import com.fourtyfourblocks.akka.testing.di.ActorPropsImpl;
import com.fourtyfourblocks.async.AsyncResponseHandler;
import com.fourtyfourblocks.thermomaster.akka.message.DeviceInfoMessage;
import com.fourtyfourblocks.thermomaster.akka.message.GetStatusResponseMessage;
import com.fourtyfourblocks.thermomaster.akka.message.ResponseStatusMessage;
import com.fourtyfourblocks.thermomaster.akka.message.SynchronizeDevicesMessage;
import com.fourtyfourblocks.thermomaster.domain.Device;
import com.fourtyfourblocks.thermomaster.domain.FileDevice;
import com.fourtyfourblocks.thermomaster.domain.Status;
import com.fourtyfourblocks.thermomaster.service.W1ThermometerService;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Provides;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import scala.concurrent.duration.FiniteDuration;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * date        : 24.09.13
 * author      : pawel
 * file name   : ThermometerMasterActorArqTest
 * <p/>
 * description :
 */
@RunWith(MockitoJUnitRunner.class)
public class ThermometerMasterActorFnlTest extends GuiceTestKitBase<ThermometerMasterActor>
{
    @Mock
    private W1ThermometerService w1service;

    @Mock
    private AsyncResponseHandler<Collection<Status>> handler;

    @Captor
    private ArgumentCaptor<Collection<Status>> captor;

    private FileDevice device;

    private Module env = new Module()
    {
        @Override
        public void configure(Binder binder)
        {
            binder.bind(Props.class)
                    .annotatedWith(new ActorPropsImpl(ThermometerSlaveActor.class))
                    .toInstance(Props.create(ForwarderActor.class, messageProbe.getRef()));
        }

        @Provides
        public W1ThermometerService getService()
        {
            return w1service;
        }
    };

    @Override
    public void before() throws Exception
    {
        getInjector(env);
        device = createDevice("test_device", "device 1");
    }

    private FileDevice createDevice(String id, String name)
    {
        return new FileDevice(id, name, Paths.get("/device/path"));
    }

    private void createSuper()
    {
        createDefaultTestActor(ThermometerMasterActor.class, "thermo_test_master");
    }

    @Test
    public void initializeMasterActor_deferInitializationForNoDevices() throws Exception
    {
        // given
        doReturn(Arrays.<Device>asList())
                .doReturn(Arrays.<Device>asList(device))
                .when(w1service).getRegisteredDevices();

        // when
        createSuper();
        messageProbe.expectNoMsg();
        watchingProbe.expectNoMsg();

        // then
        messageProbe.expectMsgClass(FiniteDuration.apply(6, TimeUnit.SECONDS), DeviceInfoMessage.class);
        messageProbe.expectNoMsg();

        verify(w1service).scan();
        verify(w1service, times(2)).getRegisteredDevices();
    }

    @Test
    public void getStatusResponseMessage() throws Exception
    {
        // given
        doReturn(Arrays.<Device>asList(device)).when(w1service).getRegisteredDevices();

        createSuper();
        messageProbe.expectMsgClass(DeviceInfoMessage.class);

        // when
        Collection<Status> statuses = assertStatuses();
        assertThat(statuses).hasSize(0);

        // when
        final Status status = new Status("20", "test status", device.getName());
        testActorRef.tell(new ResponseStatusMessage(status), messageProbe.getRef());

        messageProbe.expectNoMsg();

        // then
        statuses = assertStatuses();
        assertThat(statuses.contains(status)).isTrue();

        verify(w1service, never()).scan();
    }

    @Test
    public void synchronizeDevices() throws Exception
    {
        // given
        doReturn(Arrays.<Device>asList(device))
                .when(w1service).getRegisteredDevices();

        createSuper();
        List<DeviceInfoMessage> msgs = expectCountMsgOfType(1, DeviceInfoMessage.class);
        assertThat(msgs.get(0).getDevice().getId()).isEqualTo("test_device");

        // when
        final FileDevice device2 = createDevice("test_device2", "device 2");
        doReturn(Arrays.<Device>asList(device, device2)).when(w1service).getRegisteredDevices();
        testActorRef.tell(new SynchronizeDevicesMessage(), messageProbe.getRef());

        // then
        expectCountMsgOfType(watchingProbe, 1, Terminated.class);
    }

    @Test
    public void ignoreInvalidMessage() throws Exception
    {
        // given
        doReturn(Arrays.<Device>asList()).when(w1service).getRegisteredDevices();
        createSuper();

        // when
        testActorRef.tell(new Object(), messageProbe.getRef());

        // then
    }

    @SuppressWarnings("unchecked")
    private Collection<Status> assertStatuses()
    {
        reset(handler);
        // when
        testActorRef.tell(new GetStatusResponseMessage(handler), messageProbe.getRef());

        // then
        messageProbe.expectNoMsg();

        verify(handler).resume(captor.capture());
        verify(handler, never()).reject(any(Throwable.class));
        verify(handler, never()).cancel(any(Integer.class));

        return captor.getValue();
    }
}
    
