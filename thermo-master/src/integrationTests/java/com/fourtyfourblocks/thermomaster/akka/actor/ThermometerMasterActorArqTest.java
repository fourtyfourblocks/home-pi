/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.actor;

import akka.actor.Props;
import akka.actor.ReceiveTimeout;
import akka.testkit.JavaTestKit;
import akka.testkit.TestActorRef;
import com.fourtyfourblocks.akka.di.ActorProps;
import com.fourtyfourblocks.akka.testing.actor.ForwarderActor;
import com.fourtyfourblocks.thermomaster.akka.message.DeviceInfoMessage;
import com.fourtyfourblocks.thermomaster.domain.Device;
import com.fourtyfourblocks.thermomaster.domain.FileDevice;
import com.fourtyfourblocks.thermomaster.service.W1ThermometerService;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.enterprise.inject.Produces;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

/**
 * date        : 24.09.13
 * author      : pawel
 * file name   : ThermometerMasterActorArqTest
 * <p/>
 * description :
 */
@Ignore
@RunWith(Arquillian.class)
public class ThermometerMasterActorArqTest extends CdiArquillianTestBase
{
    private FileDevice device;

    @Deployment
    public static JavaArchive deployment()
    {
        return getArchive(ThermometerMasterActor.class);
    }

    @Produces
    @ActorProps(actorClass = ThermometerSlaveActor.class)
    public Props createSlaveProps(JavaTestKit probe)
    {
        return Props.create(ForwarderActor.class, probe.getRef());
    }

    @Produces
    public W1ThermometerService createW1Service()
    {
        return w1service;
    }

    private TestActorRef<ThermometerMasterActor> masterActorRef;
    private static final W1ThermometerService w1service = mock(W1ThermometerService.class, "w1 service");

    @Before
    public void setUp() throws Exception
    {
        device = new FileDevice("did", "device 1", Paths.get("/device/path"));

        reset(w1service);
        doReturn(Arrays.<Device>asList()).when(w1service).getRegisteredDevices();

        masterActorRef = createActor(ThermometerMasterActor.class, "master");
    }

    @Test
    public void initializeMasterActor_deferInitializationForNoDevices() throws Exception
    {
        probe.expectNoMsg();

        // trigger timeout
        doReturn(Arrays.<Device>asList(device)).when(w1service).getRegisteredDevices();
        masterActorRef.tell(ReceiveTimeout.getInstance(), probe.getRef());

        System.out.println("veryfing");
        verify(w1service).scan();
        probe.expectMsgClass(DeviceInfoMessage.class);
    }
}
    
