/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.actor;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.testkit.JavaTestKit;
import com.fourtyfourblocks.akka.di.ActorProps;
import com.fourtyfourblocks.akka.di.DiAwareCreatorFactory;
import com.fourtyfourblocks.akka.testing.GuiceTestKitBase;
import com.fourtyfourblocks.akka.testing.actor.KaboomActor;
import com.fourtyfourblocks.akka.testing.di.ActorPropsImpl;
import com.fourtyfourblocks.async.AsyncResponseHandler;
import com.fourtyfourblocks.thermomaster.akka.message.DeviceInfoMessage;
import com.fourtyfourblocks.thermomaster.akka.message.GetStatusResponseMessage;
import com.fourtyfourblocks.thermomaster.akka.message.ResponseStatusMessage;
import com.fourtyfourblocks.thermomaster.akka.message.SynchronizeDevicesMessage;
import com.fourtyfourblocks.thermomaster.domain.Device;
import com.fourtyfourblocks.thermomaster.domain.FileDevice;
import com.fourtyfourblocks.thermomaster.domain.Status;
import com.fourtyfourblocks.thermomaster.service.W1ThermometerService;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Provides;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * date        : 24.09.13
 * author      : pawel
 * file name   : ThermometerMasterActorArqTest
 * <p/>
 * description :
 */
@RunWith(MockitoJUnitRunner.class)
public class ThermometerSuperActorFnlTest extends GuiceTestKitBase<ThermometerSuperActor>
{
    @Mock
    private W1ThermometerService w1service;

    @Mock
    private AsyncResponseHandler<Collection<Status>> handler;

    @Captor
    private ArgumentCaptor<Collection<Status>> captor;

    private FileDevice device;
    private FileDevice device2;

    private Module env = new Module()
    {
        @Override
        public void configure(Binder binder)
        {
            binder.bind(Props.class)
                    .annotatedWith(new ActorPropsImpl(ThermometerSlaveActor.class))
                    .toInstance(Props.create(KaboomActor.class, messageProbe.getRef()));
        }

        @Provides
        @ActorProps(actorClass = ThermometerMasterActor.class)
        public Props getMasterProps(DiAwareCreatorFactory factory)
        {
            return Props.create(factory.creator(ThermometerMasterActor.class));
        }

        @Provides
        public W1ThermometerService getService()
        {
            return w1service;
        }
    };

    private ActorRef masterActorRef;

    public void before() throws Exception
    {
        getInjector(env);

        device = createDevice("test_device", "device 1");
        device2 = createDevice("test_device2", "device 2");
        doReturn(Arrays.<Device>asList(device)).
                doReturn(Arrays.<Device>asList(device, device2)).
                when(w1service).getRegisteredDevices();
    }

    @Override
    protected void after(JavaTestKit watchingProbe) throws Exception
    {
        watchingProbe.unwatch(masterActorRef);
    }

    private FileDevice createDevice(String id, String name)
    {
        return new FileDevice(id, name, Paths.get("/device/path"));
    }

    @Test
    public void getStatusAndSynchronize() throws Exception
    {
        // given
        createSuper();
        messageProbe.expectMsgClass(ActorRef.class);
        messageProbe.expectMsgClass(DeviceInfoMessage.class);

        // when
        Collection<Status> statuses = assertStatuses();
        assertThat(statuses).hasSize(0);

        // when
        final Status status = new Status("20", "test status", device.getName());
        masterActorRef.tell(new ResponseStatusMessage(status), messageProbe.getRef());

        messageProbe.expectNoMsg();

        // then
        statuses = assertStatuses();
        assertThat(statuses).hasSize(1);
        assertThat(statuses.iterator().next()).isSameAs(status);

        testActorRef.tell(new SynchronizeDevicesMessage(), messageProbe.getRef());
        Object[] msgs = messageProbe.receiveN(4);
        assertThat(findAll(msgs, DeviceInfoMessage.class)).hasSize(2);
        assertThat(findAll(msgs, ActorRef.class)).hasSize(2);
        watchingProbe.expectNoMsg();

        statuses = assertStatuses();
        assertThat(statuses).hasSize(0);

        final Status status2 = new Status("21", "test status 2", device2.getName());
        masterActorRef.tell(new ResponseStatusMessage(status), messageProbe.getRef());
        masterActorRef.tell(new ResponseStatusMessage(status2), messageProbe.getRef());

        statuses = assertStatuses();
        assertThat(statuses).hasSize(2);
        assertThat(statuses.contains(status)).isTrue();
        assertThat(statuses.contains(status2)).isTrue();

        verify(w1service, never()).scan();
        verify(w1service, times(2)).getRegisteredDevices();
    }

    private void createSuper()
    {
        createDefaultTestActor(ThermometerSuperActor.class, "thermo_test_super");
        masterActorRef = testActorRef.underlyingActor().getMaster();
        watchingProbe.watch(masterActorRef);
    }

    @Test
    public void handleSlaveDeath() throws Exception
    {
        // given
        createSuper();
        Object[] msgs = messageProbe.receiveN(2);
        assertThat(findAll(msgs, DeviceInfoMessage.class)).hasSize(1);

        List<ActorRef> slaves = findAll(msgs, ActorRef.class);
        assertThat(slaves).hasSize(1);

        // when
        slaves.get(0).tell(new RuntimeException("test slave exception"), messageProbe.getRef());

        // then
        watchingProbe.expectNoMsg();

        msgs = messageProbe.receiveN(4);
        assertThat(findAll(msgs, DeviceInfoMessage.class)).hasSize(2);
        assertThat(findAll(msgs, ActorRef.class)).hasSize(2);

        // then
        Collection<Status> statuses = assertStatuses();
        assertThat(statuses).hasSize(0);

        verify(w1service, never()).scan();
        verify(w1service, times(2)).getRegisteredDevices();
    }

    @SuppressWarnings("unchecked")
    private Collection<Status> assertStatuses()
    {
        reset(handler);
        // when
        testActorRef.tell(new GetStatusResponseMessage(handler), messageProbe.getRef());

        // then
        messageProbe.expectNoMsg();

        verify(handler).resume(captor.capture());
        verify(handler, never()).reject(any(Throwable.class));
        verify(handler, never()).cancel(any(Integer.class));

        return captor.getValue();
    }
}
    
