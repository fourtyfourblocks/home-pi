/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.cdi;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.fourtyfourblocks.akka.Actor;
import com.fourtyfourblocks.akka.Behaviour;
import com.fourtyfourblocks.akka.GenericActor;
import com.fourtyfourblocks.akka.SupervisorStrategyFactory;
import com.fourtyfourblocks.akka.TypeHelper;
import com.fourtyfourblocks.akka.di.ActorProps;
import com.fourtyfourblocks.akka.di.DiAwareActor;
import com.fourtyfourblocks.akka.di.cdi.ActorPropsFactory;
import com.fourtyfourblocks.akka.di.cdi.CdiAwareCreator;
import com.fourtyfourblocks.akka.di.cdi.CdiAwareCreatorFactory;
import com.fourtyfourblocks.thermomaster.akka.actor.ThermometerMasterActor;
import com.fourtyfourblocks.thermomaster.service.W1ThermometerService;
import com.fourtyfourblocks.utils.cdi.SystemProperty;
import com.fourtyfourblocks.utils.cdi.SystemPropertyFactory;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.inject.Named;

import static org.junit.Assert.assertNotNull;

/**
 * date        : 08.02.14
 * author      : pawel
 * file name   : CdiAwareWorldArqTest
 * <p/>
 * description :
 */
@RunWith(Arquillian.class)
public class CdiAwareWorldArqTest
{
    public static JavaArchive getArchive(Class... classes)
    {
        return ShrinkWrap.create(JavaArchive.class, "testE2E.jar")
                .addClasses(classes)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Deployment
    public static JavaArchive deployment()
    {
        return getArchive(
                SystemProperty.class, SystemPropertyFactory.class, ActorProps.class, ActorPropsFactory.class,
                CdiAwareWorld.class, CdiAwareCreatorFactory.class, CdiAwareCreator.class,
                Actor.class, Behaviour.class, DiAwareActor.class, GenericActor.class, TypeHelper.class,
                SupervisorStrategyFactory.class,
                ThermometerMasterActor.class, W1ThermometerService.class
        );
    }

    @Inject
    @Named("system")
    private ActorSystem postingSystem;

    @Inject
    @Named("thermometerSuper")
    private ActorRef postingSuper;


    @Test
    public void createWorld() throws Exception
    {
        assertNotNull(postingSystem);
        assertNotNull(postingSuper);
    }
}
    
