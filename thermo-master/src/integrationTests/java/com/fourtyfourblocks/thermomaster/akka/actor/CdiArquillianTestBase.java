/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.thermomaster.akka.actor;

import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.JavaTestKit;
import akka.testkit.TestActorRef;
import com.fourtyfourblocks.akka.Actor;
import com.fourtyfourblocks.akka.Behaviour;
import com.fourtyfourblocks.akka.GenericActor;
import com.fourtyfourblocks.akka.SupervisorStrategyFactory;
import com.fourtyfourblocks.akka.TypeHelper;
import com.fourtyfourblocks.akka.di.ActorProps;
import com.fourtyfourblocks.akka.di.DiAwareActor;
import com.fourtyfourblocks.akka.di.DiAwareCreatorFactory;
import com.fourtyfourblocks.akka.di.cdi.CdiAwareCreator;
import com.fourtyfourblocks.akka.di.cdi.CdiAwareCreatorFactory;
import com.fourtyfourblocks.akka.testing.cdi.CdiTestingWorld;
import com.fourtyfourblocks.utils.cdi.SystemProperty;
import com.fourtyfourblocks.utils.cdi.SystemPropertyFactory;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * date        : 11.03.14
 * author      : pawel
 * file name   : CdiArquillianTestBase
 * <p/>
 * description :
 */
public abstract class CdiArquillianTestBase
{
    public static JavaArchive getArchive(Class... classes)
    {
        final List<Class> c = Arrays.asList(standardClasses());
        final List<Class> l = Arrays.asList(classes);

        List<Class> n = new ArrayList<>(c);
        n.addAll(l);

        return ShrinkWrap.create(JavaArchive.class, "testE2E.jar")
                .addClasses(n.toArray(new Class[n.size()]))
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    public static Class[] standardClasses()
    {
        return new Class[]{
                JavaTestKit.class,
                SystemProperty.class, SystemPropertyFactory.class, ActorProps.class,
                CdiTestingWorld.class, DiAwareCreatorFactory.class, CdiAwareCreatorFactory.class, CdiAwareCreator.class,
                Actor.class, Behaviour.class, DiAwareActor.class, GenericActor.class, TypeHelper.class,
                SupervisorStrategyFactory.class
        };
    }

    @Inject
    @Named("system")
    public ActorSystem system;

    @Inject
    public DiAwareCreatorFactory factory;

    @Inject
    public JavaTestKit probe;

    @After
    public void tearDown() throws Exception
    {
        system.shutdown();
    }

    public <T extends GenericActor> TestActorRef<T> createActor(Class<T> clazz, String actorName)
    {
        final Props props = Props.create(factory.creator(clazz));
        final TestActorRef<T> actorRef = TestActorRef.create(system, props, actorName);
        probe.watch(actorRef);

        return actorRef;
    }
}
    
