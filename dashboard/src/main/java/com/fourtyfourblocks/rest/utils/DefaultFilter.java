/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.rest.utils;

import com.fourtyfourblocks.akka.messages.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;

/**
 * date        : 24.11.13
 * author      : pawel
 * file name   : DefaultFilter
 * <p/>
 * description :
 */
@WebFilter(asyncSupported = true, urlPatterns = "/*", filterName = "default-filter")
public class DefaultFilter implements Filter
{
    private final static Logger logger = LoggerFactory.getLogger(DefaultFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        logger.debug("Initializing filter");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
                                                                                                     ServletException
    {
        long start = System.currentTimeMillis();
        MDC.put(Message.MDC_LOGGER_KEY, UUID.randomUUID().toString());

        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        final StringBuffer sb = httpRequest.getRequestURL();
        String queryString = httpRequest.getQueryString();
        if (queryString != null)
        {
            sb.append('?').append(queryString);
        }

        boolean wasException = false;
        try
        {
            chain.doFilter(request, response);
        }
        catch (IOException | ServletException e)
        {
            wasException = true;
            throw e;
        }
        finally
        {
            if (wasException)
            {
                logger.error("[failed][timetaken: {} ms] {}", (System.currentTimeMillis() - start), sb.toString());
            }
            else
            {
                logger.info("[time: {} ms] {}", (System.currentTimeMillis() - start), sb.toString());
            }
        }
    }

    @Override
    public void destroy()
    {
        logger.debug("Destroying filter");
    }
}
    
