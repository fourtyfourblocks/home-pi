/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

/**
 * date        : 24.11.13
 * author      : pawel
 * file name   : AsyncRestController
 * <p/>
 * description :
 */
@Path("/")
public class AsyncRestController
{

    @GET
    @Path("job/{name}/async")
    @Produces(MediaType.APPLICATION_JSON)
    public void getJob(@PathParam("name") final String name,
                       @Suspended final AsyncResponse response) throws Exception
    {
//        client.getJob(name, new JsonAsyncResponseHandler<>(response, Job.class));
    }

    @GET
    @Path("job/{name}/tell_async")
    @Produces(MediaType.APPLICATION_JSON)
    public void getJobTell(@PathParam("name") final String name,
                           @Suspended final AsyncResponse response) throws Exception
    {
//        client.getJobTell(name, new JsonAsyncResponseHandler<>(response, Job.class));
    }

    @GET
    @Path("job/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getBasic(@PathParam("name") String name) throws Exception
    {
//        final Job job = new Job();
//        job.setName(name);
//        job.setIdentifier(UUID.randomUUID().toString());
//        job.setExternalId(UUID.randomUUID().toString());
//        job.setCompanyExternalId(UUID.randomUUID().toString());
//        job.setCreatorExternalId(UUID.randomUUID().toString());
//        job.setJobType(JobType.NORMAL);
//        job.setCreateDate(new Date());
//        job.setFunction(new Function("id", "id", "A function"));
//        job.setIndustry(new Industry("id", "id", "A industry"));
//        job.setHiringTeamIds(new ArrayList<Long>());
//        job.setLocation(new Location("US", "CA", "San Francisco"));
//
        return null;
    }
}
    
