/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.rest.async;

import com.fourtyfourblocks.async.AsyncResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.TimeUnit;

/**
 * date        : 26.11.13
 * author      : pawel
 * file name   : JsonAsyncResponseHandler
 * <p/>
 * description :
 */
public class JsonAsyncResponseHandler<T> implements AsyncResponseHandler<T>
{
    private final static Logger logger = LoggerFactory.getLogger(JsonAsyncResponseHandler.class);

    private final AsyncResponse asyncResponse;
    private final Class<T> clazz;

    public JsonAsyncResponseHandler(AsyncResponse asyncResponse, Class<T> clazz)
    {
        this.asyncResponse = asyncResponse;
        asyncResponse.setTimeout(2, TimeUnit.SECONDS);
        this.clazz = clazz;
    }

    @Override
    public void resume(T value)
    {
        final Response response = Response.
                ok(value).
                header("Content-Type", MediaType.APPLICATION_JSON).
                build();
        asyncResponse.resume(response);
    }

    @Override
    public void reject(Throwable reason)
    {
        asyncResponse.resume(reason);
    }

    @Override
    public void cancel(int retryAfter)
    {
        asyncResponse.cancel(retryAfter);
    }

    @Override
    public Class<T> getType()
    {
        return clazz;
    }
}
    
