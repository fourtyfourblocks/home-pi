/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * date        : 24.11.13
 * author      : pawel
 * file name   : AkkaApp
 * <p/>
 * description :
 */
@ApplicationPath("/")
public class AkkaApp extends Application
{
    private final static Logger logger = LoggerFactory.getLogger(AkkaApp.class);

    public AkkaApp()
    {
        logger.info("creating app");
    }
}
    
